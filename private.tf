/*
  Database Servers
*/
resource "aws_security_group" "db" {
    name = "vpc_db"
    description = "Allow incoming database connections."

    ingress { # Redis
        from_port = 6379
        to_port = 6379
        protocol = "tcp"
        cidr_blocks = ["${var.vpc_cidr}"]
    }
    ingress { # Postgresql
        from_port = 5432
        to_port = 5432
        protocol = "tcp"
        cidr_blocks = ["${var.vpc_cidr}"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.vpc_cidr}"]
    }
    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["${var.vpc_cidr}"]
    }

    egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }

    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "DBServerSG"
    }
}

resource "aws_instance" "db-1" {
    ami = "${lookup(var.aws_ami_ubuntu_precise, var.aws_region)}"
    availability_zone = "${var.aws_az}"
    instance_type = "m1.small"
    key_name = "${var.aws_key_name}"
    security_groups = ["${aws_security_group.db.id}"]
    subnet_id = "${aws_subnet.us-west-2-private.id}"
    source_dest_check = false
    private_ip = "10.0.2.5"

    tags {
        Name = "DB Server 1"
    }
}

