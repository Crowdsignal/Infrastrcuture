sudo yum update
mkdir install
cd install
sudo yum install git gcc-c++ openssl-devel python
sudo pip install ansible==2.1.2.0
wget https://www.agwa.name/projects/git-crypt/downloads/git-crypt-0.5.0.tar.gz
cd git-crypt-0.5.0
make
sudo make install
cd ..
git clone git@gitlab.com:Crowdsignal/Infrastrcuture.git
git clone git@gitlab.com:Crowdsignal/cs.keys.git
cd Infrastrcuture
git-crypt unlock ../cs.keys/Infrastructure_gitcrypt_keyfile
sh extensions/setup/role_update.sh
cd plays
#ansible-playbook -i ../production.ini db.yml
#ansible-playbook -i ../production.ini ti.yml