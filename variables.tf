variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_key_path" {}
variable "aws_key_name" {}

variable "aws_region" {
    description = "AWS Region for the VPC"
    default = "us-west-2"
}

variable "aws_az" {
    description = "AWS Avaiablity Zone for the VPC"
    default = "us-west-2b"
}

# Ubuntu Precise 12.04 LTS (x64)
variable "aws_ami_ubuntu_precise" {
    default = {
        eu-west-1 = "ami-b1cf19c6"
        us-east-1 = "ami-de7ab6b6"
        us-west-1 = "ami-3f75767a"
        us-west-2 = "ami-21f78e11"
    }
}

# Ubuntu Precise 12.04 LTS (x64)
variable "aws_ami_nat" {
    default = {
        us-west-2 = "ami-a275b1c2"
    }
}

variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
    description = "CIDR for the Public Subnet"
    default = "10.0.1.0/24"
}

variable "private_subnet_cidr" {
    description = "CIDR for the Private Subnet"
    default = "10.0.2.0/24"
}
