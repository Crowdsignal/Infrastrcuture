/*
 Tweet Ingestors
*/
resource "aws_security_group" "ti" {
    name = "vpc_ti"
    description = "Tweet Ingestors."

    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.vpc_cidr}"]
    }

    egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }

    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "TweetIngestorSG"
    }
}

resource "aws_instance" "ti-1" {
    ami = "ami-8bc165eb" # Ubuntu Precise, EBS, HVM
    availability_zone = "${var.aws_az}"
    instance_type = "t2.nano"
    key_name = "${var.aws_key_name}"
    security_groups = ["${aws_security_group.ti.id}"]
    subnet_id = "${aws_subnet.us-west-2-public.id}"
    associate_public_ip_address = true
    source_dest_check = false
    private_ip = "10.0.1.5"

    tags {
        Name = "Tweet Ingestor 1"
    }
}

resource "aws_instance" "ti-2" {
    ami = "ami-8bc165eb" # Ubuntu Precise, EBS, HVM
    availability_zone = "${var.aws_az}"
    instance_type = "t2.nano"
    key_name = "${var.aws_key_name}"
    security_groups = ["${aws_security_group.ti.id}"]
    subnet_id = "${aws_subnet.us-west-2-public.id}"
    associate_public_ip_address = true
    source_dest_check = false
    private_ip = "10.0.1.6"

    tags {
        Name = "Tweet Ingestor 2"
    }
}
